﻿using MOVIEAPP.Models;
using System.Collections.Generic;
using System.Linq;

namespace MOVIEAPP.Data
{
    public static class MovieRepository
    {
        private static List<Movie> _movies = null;

        static MovieRepository()
        {
            _movies = new List<Movie>()
            {
                new Movie(){ Id=1, Name="Malefique", ShortDescription="Kötü Kadın",  Description="<p>Fantastik kötü kadının Maceraları (kanatları yokmuydu?!)</p>", ImageUrl = "1.jpg", CategoryId = 3},
                new Movie(){ Id=2, Name="The Godfather", ShortDescription="Mafya filmi", Description="<p>Mafya filmlerinin baş yapıtı.</p>", ImageUrl = "2.jpg", CategoryId = 5},
                new Movie(){ Id=3, Name="Gravity", ShortDescription="Bir uzay filmi", Description="<p>Uzayda ölüm kalım mücadelesi filmi.</p>", ImageUrl = "3.jpg", CategoryId = 1},
                new Movie(){ Id=4, Name="Xmen", ShortDescription="Fantastik bilim kurgu", Description="<p>Mutantların fantastik maceralarını konu ediyor.</p>", ImageUrl = "4.jpg", CategoryId = 3},
                new Movie(){ Id=5, Name="Limitless", ShortDescription="Zeka filmi", Description="<p>Zihni kuvvetlendiren bir hap alınca bütün dünyası değişiyor.(Keşke o haplardan bende de olsa!)</p>", ImageUrl = "5.jpg", CategoryId = 1}
            };
        }

        public static List<Movie> Movies
        {
            get
            {
                return _movies;
            }
        }

        public static void AddMovie(Movie entity)
        {
            _movies.Add(entity);
        }

        public static Movie GetById(int id)
        {
            return _movies.FirstOrDefault(s => s.Id == id);
        }
    }
}
