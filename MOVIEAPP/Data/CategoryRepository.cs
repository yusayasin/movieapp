﻿using MOVIEAPP.Models;
using System.Collections.Generic;
using System.Linq;

namespace MOVIEAPP.Data
{
    public class CategoryRepository
    {
        private static List<Category> _categories = null;

        static CategoryRepository()
        {
            _categories = new List<Category>()
            {
                new Category(){Id=1, Name="Macera"},
                new Category(){Id=2, Name="Bilimkurgu"},
                new Category(){Id=3, Name="Fantastik"},
                new Category(){Id=4, Name="Komedi"},
                new Category(){Id=5, Name="Dram"}
            };
        }

        public static List<Category> Categories
        {
            get
            {
                return _categories;
            }
        }

        public static void AddCategory(Category entity)
        {
            _categories.Add(entity);
        }

        public static Category GetById(int id)
        {
            return _categories.FirstOrDefault(s => s.Id == id);
        }
    }
}
