﻿using Microsoft.AspNetCore.Mvc;
using MOVIEAPP.Data;
using System;

namespace MOVIEAPP.ViewComponents
{
    public class CategoryMenuViewComponent : ViewComponent
    {
        public IViewComponentResult Invoke()
        {
            if(RouteData?.Values["action"].ToString() == "Details")
            {
                ViewBag.SelectedCategory = MovieRepository.GetById(Convert.ToInt32(RouteData?.Values["id"].ToString())).CategoryId.ToString();
            }
            else
            {
                ViewBag.SelectedCategory = RouteData?.Values["id"];
            }


            return View(CategoryRepository.Categories);
        }
    }
}
