﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MOVIEAPP.Data;
using MOVIEAPP.Models;

namespace MOVIEAPP.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public IActionResult Index(int? id)
        {
            //ProductCategoryModel productCategory = new ProductCategoryModel();
            //productCategory.Movies = MovieRepository.Movies;
            //productCategory.Categories = CategoryRepository.Categories;
            List<Movie> movies = MovieRepository.Movies;
            if(id != null)
            {
                movies = movies.Where(s => s.CategoryId == id).ToList();
            }

            return View(movies);
        }

        public IActionResult Details(int id)
        {
            //ProductCategoryModel model = new ProductCategoryModel();
            //model.Categories = CategoryRepository.Categories;
            //model.Movie = MovieRepository.GetById(id);
            return View(MovieRepository.GetById(id));
        }

        public IActionResult Contact()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
